export const EN =
{
  //A
  "accept": "Accept",
  "add": "Add",
  "added.admin": "Added by admin",
  "admin": "Admin",
  "all": "All",
  "all.the": "All the",
  "anonymous": "Anonymous",
  "anonymous.email": "Anonymous with email",
  "anonymous.link": "Anonymous with link",
  "around": "around",
  "around.map.center": "around map center",

  //B
  "back.to.default.view": "Back to Default View",
  "back.to.map": "Back To Map",
  "by": "By",

  //C
  "cancel": "Cancel",
  "can.not.locate.address": "Can not locate this address !",
  "card": "Card",
  "change.tiles": "Change Tiles Layer",
  "check.uncheck.all": "Check/Uncheck All",
  "close": "Close",
  "close.menu": "Close Menu",
  "collaborative.moderation": "Collaborative moderation",
  "collaborative.moderation.first.paragraph": "When an element is added or modified, the update of the data is not instantaneous. The element will initially appear 'grayed' on the map, and it will then be possible for all users logged to vote once and for this item. This vote is not an opinion, but a sharing of knowledge. If you know this element, or know that this element does not exist, then your knowledge interests us!",
  "collaborative.moderation.second.paragraph": "After a certain number of votes, the element can then be automatically validated or refused. In case of dispute (both positive and negative votes), a moderator will intervene as soon as possible. We count on you!",
  "comment.for.moderation": "Comment for moderation",
  "copy.html.code": "Copy this HTML code into your website !",

  //D
  "decide": "Decide",
  "decide.for": "Decide for",
  "does.not.exist": "Does not exist",
  "does.not.respect.charter": "Does not respect the charter",
  "does.not.respect.charter.nothing.to.do.here": "does not respect the charter, it has nothing to do here",
  "duplicate.on.map": "is referenced several times on the map (duplicate)",

  //E
  "edit": "Edit",
  "element": "element",
  "element.definite": "the element",
  "element.indefinite": "an element",
  "element.no.longer.exists": "The element no longer exists",
  "element.no.respect.charter": "The element does not respect the charter",
  "element.plural": "elements",
  "element.referenced.several.times": "The element is referenced several times",
  "email.will.be.sent.to": "An email will be sent to ",
  "email.content": "Message content",
  "enter.an.address.postal.code.city": "Enter an address, a postal code, a city...",
  "enter.the.name.of": "Enter the name of ",
  "enter.valid.email": "Please enter a valid email address",
  "error.occured": "Sorry, an error occurred",
  "error.occurend.route.calculation": "An error occurred while calculating the route, sorry!",
  "errors.reported": "Errors reported",
  "exists": "Exists",
  "exists.and.correct.informations": "Exists and informations are correct",
  "exists.but.given.informations.are.incorrect": "exists but the given informations are incorrect",
  "exists.but.i.do.not.know.anything.else.about.it": "exists, but I do not know anything else about it",
  "exists.and.i.validate.the.accuracy.of.the.given.informations": "exists, and I validate the accuracy of the given informations",
  "exists.but.incorrect.informations": "Exists but informations are incorrect",
  "export.iframe": "Export Iframe",

  //F
  "files": "Files",
  "fill.fields.below": "You must fill in the fields below",
  "find.route": "Find the route",
  "find.a.place": "Find a place",
  "found.one.or.several" : "found",

  //G
  "general.infos": "General Informations",
  "geolocalize": "Geolocalize your position",
  "geolocation.error": "Geolocation error",

  //H
  "height": "Height",
  "historical": "Historical",

  //I
  "i.do.not.agree.with.the.proposed.modifications": "I do not agree with the proposed modifications",
  "i.validate.the.proposed.modifications": "I validate the proposed modifications",
  "iframe.initialized.position.map": "The iframe will be initialized in the current position of the map. Move the map to the desired position before opening this window !",
  "import": "Import",
  "informations.incorrect": "The informations are incorrect",

  //L
  "label": "Label",
  "laozi": "Laozi",
  "laozi.quotation": "« Failure is the foundation of success. »",
  "list.of": "List of",
  "loading": "Loading...",
  "logged.user": "Logged user",

  //M
  "map.of": "Map of",
  "mark.as.resolved": "Mark as resolved",
  "menu": "menu",
  "modification": "Modification",
  "modification.plural": "Modifications",
  "modified.admin": "Modified by admin",
  "modified.direct.link": "Modified with direct link",
  "modified.owner": "Modified by owner",
  "modified.pending": "Modified pending",
  "my.position": "my position",

  //N
  "new": "New ",
  "no.category.provided": "No Category provided",
  "no.longer.exist": "no longer exist",
  "no.moderation.necessary": "No moderation necessary",
  "no.result.found": "No result found",
  "non.consensual.votes": "Non-consensual votes",
  "not.correct.informations": "The informations are incorrect (if you can correct them, please do so via the button 'Propose changes')",

  //O
  "ok": "OK!",
  "openhours": "Open Hours",
  "other.infos": "Other Informations",

  //P
  "pending.add": "Pending (add)",
  "pending.for.too.long": "Pending for too long",
  "pending.modifications": "Pending (modifications)",
  "pending.validation": "Pending for validation",
  "place": "A place",
  "placeholder.input.comment": "Proof in case of refusal (which will be inserted in the automatic email)",
  "placeholder.input.comment.administration": "Optional comment for moderation",
  "placeholder.input.subject.email": "Message subject",
  "potential.duplicate": "Potential duplicate",
  "propose.changes": "Propose changes",
  "proposed.by": "Proposed by",

  //R
  "refused.admin": "Refused (admin)",
  "refused.votes": "Refused (votes)",
  "reject": "Reject",
  "rejected.or.deleted.element": "(This element has been rejected or deleted)",
  "remove": "Remove",
  "remove.from.favorites": "Remove from favorites",
  "removed": "Removed",
  "report": "Report",
  "report.error": "Report an error",
  "report.error.regarding": "Report an error regarding",
  "reporting": "Reporting",
  "results": "Results",
  "route": "Route",
  "route.calculation": "Route calculation",
  "route.to.element": "Route to this element",

  //S
  "save": "Save",
  "saved": "Saved",
  "save.as.favorites": "Save as favorites",
  "search": "Search",
  "search.for": "Search for",
  "see.on.map": "See on the map",
  "select.type.error": "You must select a type of error!",
  "send": "Send",
  "send.email": "Send an email",
  "send.email.to": "Send an email to ",
  "share.link": "Share this link with others",
  "share.url": "Share",
  "share.your.knowledge.about": "Share your knowledge about",
  "show.as.list": "Show as List",
  "show.hide.detail": "Show/Hide Detail",
  "show.more": "Show more",
  "show.on.map": "Show on the map",
  "show.only": "Show only",
  "show.only.actors.with.label": "Show only actors with the label",
  "show.only.elements.to.moderate":"Show only elements to moderate",
  "show.only.elements.validation.process": "Show only elements in the process of validation",
  "show.only.favorites" : "Show only favorites",
  "show.only.selected.categories": "Show only currently selected categories",
  "source": "Source",
  "starting.address": "Starting address",
  "starting.address.title": "Enter a starting address",

  //T
  "the": "the",
  "the.addition": "the addition",
  "to.moderate": "To moderate",
  "type.message.here": "Type your message here",

  //U
  "updated.at": "Updated At",

  //V
  "validated.admin": "Validated (admin)",
  "validated.votes": "Validated (votes)",
  "video": "Video",
  "vote": "Vote",

  //W
  "waiting": "Waiting",
  "what.error.to.report": "What error do you want to report ?",
  "width": "Width",

  //Y
  "you.must.select.your.vote": "You must select your vote !",
  "your.email": "Your email address",
  "your.favorites": "Favorites"
}